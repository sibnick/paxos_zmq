module paxos.cluster;

enum Status { LIVE, SUSPECT, DIED }

struct Node
{
    string host;
    Status status;
}

enum HEARTBEAT_TIMEOUT = 10_000_000;

import zmqd;
import std.exception, std.conv:to;
import std.algorithm.comparison:min,max;
import std.experimental.logger;

class BebCluster
{
    private
    {
        string url;
        string returnUrl;
        Socket socket;
    }

    public
    {
        this(string url)
        {
            this.url = url;
            socket = Socket(SocketType.req);
            socket.connect(url);
        }

        void close()
        {
            socket.close();
        }

        void bebSend(ubyte[] msg)
        {
            socket.send(msg);
        }

        ubyte[] bebReceive()
        {
            return sRecv(socket);
        }
    }
}

BebCluster startBebCluster(string meSelf, immutable(string[]) hosts)
{
    import std.concurrency;
    auto url = "inproc://cluster";
    spawn(&runCluster, url, meSelf, hosts);
    return new BebCluster(url);
}

private
{
    enum MessageType
    {
        HEARTBEAT, HEARTBEAT_ACK, DATA //, DATA_ACK, JOIN, LEAVE
    }

    struct TransportMessage
    {
        ulong id;
        string node;
        MessageType msgType;
        ubyte[] data;
    }

    void runCluster(string url, string meSelf, const string[] hosts)
    {
        log("run");
        auto router = Socket(SocketType.router);
        import core.time, core.thread;
        router.bind(url);
        HarmonyCluster cluster = new HarmonyCluster(meSelf, hosts);
        auto items = [
            PollItem(router, PollFlags.pollIn),
            PollItem(cluster.router, PollFlags.pollIn),
        ];
        alias message_responses = bool[ulong];
        message_responses[uint] identities;
        while(true)
        {
            const n = poll(items, 1000.msecs);
            if (items[0].returnedEvents & PollFlags.pollIn) {
                try {
                    auto data = sRecv(router);
                    logf("head data: %s", data[0..min(5, data.length)]);
                    uint identity = *cast(uint*)(data.ptr+1);
                    auto msgId = cluster.bebSend(data);
                    identities[identity][msgId] = true;
                } catch (Exception e)
                {
                    error(e);
                }
            }
            if (items[1].returnedEvents & PollFlags.pollIn) {
                try {
                    auto data = cluster.bebRecieveImpl();
                    logf("head data: %s", data[0..min(5, data.length)]);
                    router.send(data);
                } catch (Exception e)
                {
                    error(e);
                }
            }
            cluster.sendHearbeats;
        }
    }

/*
The Harmony pattern for a true peer network:
   - one ROUTER for input from all peers, and one DEALER per peer for output.
   - Bind the ROUTER, connect the DEALER, and start each conversation with
     an OHAI equivalent that provides the return IP address and port
*/
    class HarmonyCluster
    {
        import msgpack;
        import std.datetime.systime:Clock;
        struct OutCon
        {
            Node node;
            Socket sock;
            long sendTime;
            long recvTime;
            long sendId;
            long recvId;
        }
        private
        {
            OutCon[string] nodes;
            Socket router;
            string meSelf;
            ulong idSeq = 1;


            auto bebSend(ulong id, ubyte[] bytes, ref OutCon outCon)
            {
                if(outCon.sendTime - outCon.recvTime > 10*HEARTBEAT_TIMEOUT)
                {
                    log("Heartbeat failed for: ", outCon.node);
                    outCon.node.status = Status.SUSPECT;
                    outCon.sock.close();
                    return false;
                }
                if(outCon.node.status!=Status.LIVE)
                {
                    logf("Skip node: %s", outCon.node);
                    return false;
                }
                outCon.sock.send(bytes);
                outCon.sendTime = Clock.currStdTime;
                outCon.sendId = id;
                return true;
            }

            auto bebSend(TransportMessage msg)
            {
                auto bytes = pack(msg);
                auto count = 0;
                foreach(ref outCon; nodes)
                    if(bebSend(msg.id, bytes, outCon))
                        count++;
                return count;
            }

            ubyte[] bebRecieveImpl()
            {
                auto msg = unpack!TransportMessage(sRecv(router)[5..$]);
                log("Recv: ", msg);
                auto outCon = &nodes[msg.node];
                outCon.recvTime = Clock.currStdTime;
                outCon.recvId = msg.id;
                if(msg.msgType==MessageType.HEARTBEAT)
                {
                    auto respMsg = TransportMessage(idSeq++, meSelf, MessageType.HEARTBEAT_ACK);
                    if(bebSend(respMsg.id, pack(respMsg), *outCon))
                        log("Send hearbeat resp: ", respMsg);
                }
                return msg.data;
            }

            auto sendHearbeats()
            {
                auto count = 0;
                auto now = Clock.currStdTime;
                foreach(pair; nodes.byKeyValue)
                {
                    if(now - pair.value.sendTime < HEARTBEAT_TIMEOUT) continue;
                    count++;
                    auto heartbeat = TransportMessage(idSeq++, meSelf, MessageType.HEARTBEAT);
                    auto bytes = pack(heartbeat);
                    if( bebSend(heartbeat.id, bytes, pair.value) )
                        log("Send hearbeat to: ", pair.key);
                }
                return count;
            }
        }

        public
        {
            this(string meSelf, const string[] hosts)
            {
                this.meSelf = meSelf;
                router = Socket(SocketType.router);
                router.bind("tcp://*:5569");
                log("Bind to router port");
                auto now = Clock.currStdTime;
                foreach(host; hosts)
                {
                    auto node = Node(host, Status.LIVE);
                    nodes[host] = OutCon(node, Socket(SocketType.dealer), now, now);
                    nodes[host].sock.connect("tcp://"~host~":5569");
                }
            }

            auto bebSend(ubyte[] data)
            {
                auto msg = TransportMessage(idSeq++, meSelf, MessageType.DATA, data);
                bebSend(msg);
                return msg.id;
            }
        }
    }


    auto sRecv(ref Socket socket)
    {
        ubyte[] data;
        Frame message;
        do {
            // Process all parts of the message
            message.rebuild();
            socket.receive(message);
            data ~= message.data();
        } while (message.more);
        return data;
    }


}