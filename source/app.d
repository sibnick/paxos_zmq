import core.thread, core.time;
import std.stdio;
import std.conv:to;
import zmqd;
import std.exception;
import std.experimental.logger;
/*
The Harmony pattern for a true peer network:
   - one ROUTER for input from all peers, and one DEALER per peer for output.
   - Bind the ROUTER, connect the DEALER, and start each conversation with
     an OHAI equivalent that provides the return IP address and port
*/
void main(string[] args)
{
    log("Starting...");
    string meSelf = args[1];
    log("Bind to router port. Me self: ", meSelf);

    import paxos.cluster;
    auto cluster = startBebCluster(meSelf, args[2..$].idup);
    if(meSelf=="node1")
    {
        cluster.bebSend(cast(ubyte[])"ABC");
        log(cluster.bebReceive().asString());
        log(cluster.bebReceive().asString());
    }else{
        //auto test = cluster.bebReceive();
       // cluster.bebSend( test~test );
    }

}
